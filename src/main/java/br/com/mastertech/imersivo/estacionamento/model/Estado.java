package br.com.mastertech.imersivo.estacionamento.model;

public enum Estado {
	
	PENDENTE, PAGO;

}
